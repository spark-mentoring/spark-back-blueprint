package com.sparkdigital.diary.web.rest;

import com.sparkdigital.diary.DiaryApp;

import com.sparkdigital.diary.domain.Horse;
import com.sparkdigital.diary.repository.HorseRepository;
import com.sparkdigital.diary.service.HorseService;
import com.sparkdigital.diary.repository.search.HorseSearchRepository;
import com.sparkdigital.diary.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.ZoneId;
import java.util.List;

import static com.sparkdigital.diary.web.rest.TestUtil.sameInstant;
import static com.sparkdigital.diary.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.sparkdigital.diary.domain.enumeration.Sex;
/**
 * Test class for the HorseResource REST controller.
 *
 * @see HorseResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = DiaryApp.class)
public class HorseResourceIntTest {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_DUE = "AAAAAAAAAA";
    private static final String UPDATED_DUE = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    private static final Sex DEFAULT_SEX = Sex.MALE;
    private static final Sex UPDATED_SEX = Sex.FEMALE;

    private static final ZonedDateTime DEFAULT_BIRTH = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_BIRTH = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    @Autowired
    private HorseRepository horseRepository;

    @Autowired
    private HorseService horseService;

    @Autowired
    private HorseSearchRepository horseSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restHorseMockMvc;

    private Horse horse;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final HorseResource horseResource = new HorseResource(horseService);
        this.restHorseMockMvc = MockMvcBuilders.standaloneSetup(horseResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Horse createEntity(EntityManager em) {
        Horse horse = new Horse()
            .name(DEFAULT_NAME)
            .due(DEFAULT_DUE)
            .description(DEFAULT_DESCRIPTION)
            .sex(DEFAULT_SEX)
            .birth(DEFAULT_BIRTH);
        return horse;
    }

    @Before
    public void initTest() {
        horseSearchRepository.deleteAll();
        horse = createEntity(em);
    }

    @Test
    @Transactional
    public void createHorse() throws Exception {
        int databaseSizeBeforeCreate = horseRepository.findAll().size();

        // Create the Horse
        restHorseMockMvc.perform(post("/api/horses")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(horse)))
            .andExpect(status().isCreated());

        // Validate the Horse in the database
        List<Horse> horseList = horseRepository.findAll();
        assertThat(horseList).hasSize(databaseSizeBeforeCreate + 1);
        Horse testHorse = horseList.get(horseList.size() - 1);
        assertThat(testHorse.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testHorse.getDue()).isEqualTo(DEFAULT_DUE);
        assertThat(testHorse.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testHorse.getSex()).isEqualTo(DEFAULT_SEX);
        assertThat(testHorse.getBirth()).isEqualTo(DEFAULT_BIRTH);

        // Validate the Horse in Elasticsearch
        Horse horseEs = horseSearchRepository.findOne(testHorse.getId());
        assertThat(testHorse.getBirth()).isEqualTo(testHorse.getBirth());
        assertThat(horseEs).isEqualToIgnoringGivenFields(testHorse, "birth");
    }

    @Test
    @Transactional
    public void createHorseWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = horseRepository.findAll().size();

        // Create the Horse with an existing ID
        horse.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restHorseMockMvc.perform(post("/api/horses")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(horse)))
            .andExpect(status().isBadRequest());

        // Validate the Horse in the database
        List<Horse> horseList = horseRepository.findAll();
        assertThat(horseList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = horseRepository.findAll().size();
        // set the field null
        horse.setName(null);

        // Create the Horse, which fails.

        restHorseMockMvc.perform(post("/api/horses")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(horse)))
            .andExpect(status().isBadRequest());

        List<Horse> horseList = horseRepository.findAll();
        assertThat(horseList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllHorses() throws Exception {
        // Initialize the database
        horseRepository.saveAndFlush(horse);

        // Get all the horseList
        restHorseMockMvc.perform(get("/api/horses?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(horse.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].due").value(hasItem(DEFAULT_DUE.toString())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())))
            .andExpect(jsonPath("$.[*].sex").value(hasItem(DEFAULT_SEX.toString())))
            .andExpect(jsonPath("$.[*].birth").value(hasItem(sameInstant(DEFAULT_BIRTH))));
    }

    @Test
    @Transactional
    public void getHorse() throws Exception {
        // Initialize the database
        horseRepository.saveAndFlush(horse);

        // Get the horse
        restHorseMockMvc.perform(get("/api/horses/{id}", horse.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(horse.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.due").value(DEFAULT_DUE.toString()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()))
            .andExpect(jsonPath("$.sex").value(DEFAULT_SEX.toString()))
            .andExpect(jsonPath("$.birth").value(sameInstant(DEFAULT_BIRTH)));
    }

    @Test
    @Transactional
    public void getNonExistingHorse() throws Exception {
        // Get the horse
        restHorseMockMvc.perform(get("/api/horses/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateHorse() throws Exception {
        // Initialize the database
        horseService.save(horse);

        int databaseSizeBeforeUpdate = horseRepository.findAll().size();

        // Update the horse
        Horse updatedHorse = horseRepository.findOne(horse.getId());
        // Disconnect from session so that the updates on updatedHorse are not directly saved in db
        em.detach(updatedHorse);
        updatedHorse
            .name(UPDATED_NAME)
            .due(UPDATED_DUE)
            .description(UPDATED_DESCRIPTION)
            .sex(UPDATED_SEX)
            .birth(UPDATED_BIRTH);

        restHorseMockMvc.perform(put("/api/horses")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedHorse)))
            .andExpect(status().isOk());

        // Validate the Horse in the database
        List<Horse> horseList = horseRepository.findAll();
        assertThat(horseList).hasSize(databaseSizeBeforeUpdate);
        Horse testHorse = horseList.get(horseList.size() - 1);
        assertThat(testHorse.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testHorse.getDue()).isEqualTo(UPDATED_DUE);
        assertThat(testHorse.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testHorse.getSex()).isEqualTo(UPDATED_SEX);
        assertThat(testHorse.getBirth()).isEqualTo(UPDATED_BIRTH);

        // Validate the Horse in Elasticsearch
        Horse horseEs = horseSearchRepository.findOne(testHorse.getId());
        assertThat(testHorse.getBirth()).isEqualTo(testHorse.getBirth());
        assertThat(horseEs).isEqualToIgnoringGivenFields(testHorse, "birth");
    }

    @Test
    @Transactional
    public void updateNonExistingHorse() throws Exception {
        int databaseSizeBeforeUpdate = horseRepository.findAll().size();

        // Create the Horse

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restHorseMockMvc.perform(put("/api/horses")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(horse)))
            .andExpect(status().isCreated());

        // Validate the Horse in the database
        List<Horse> horseList = horseRepository.findAll();
        assertThat(horseList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteHorse() throws Exception {
        // Initialize the database
        horseService.save(horse);

        int databaseSizeBeforeDelete = horseRepository.findAll().size();

        // Get the horse
        restHorseMockMvc.perform(delete("/api/horses/{id}", horse.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean horseExistsInEs = horseSearchRepository.exists(horse.getId());
        assertThat(horseExistsInEs).isFalse();

        // Validate the database is empty
        List<Horse> horseList = horseRepository.findAll();
        assertThat(horseList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchHorse() throws Exception {
        // Initialize the database
        horseService.save(horse);

        // Search the horse
        restHorseMockMvc.perform(get("/api/_search/horses?query=id:" + horse.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(horse.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].due").value(hasItem(DEFAULT_DUE.toString())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())))
            .andExpect(jsonPath("$.[*].sex").value(hasItem(DEFAULT_SEX.toString())))
            .andExpect(jsonPath("$.[*].birth").value(hasItem(sameInstant(DEFAULT_BIRTH))));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Horse.class);
        Horse horse1 = new Horse();
        horse1.setId(1L);
        Horse horse2 = new Horse();
        horse2.setId(horse1.getId());
        assertThat(horse1).isEqualTo(horse2);
        horse2.setId(2L);
        assertThat(horse1).isNotEqualTo(horse2);
        horse1.setId(null);
        assertThat(horse1).isNotEqualTo(horse2);
    }
}
