package com.sparkdigital.diary.web.rest;

import com.sparkdigital.diary.DiaryApp;

import com.sparkdigital.diary.domain.CategoryMovement;
import com.sparkdigital.diary.repository.CategoryMovementRepository;
import com.sparkdigital.diary.service.CategoryMovementService;
import com.sparkdigital.diary.repository.search.CategoryMovementSearchRepository;
import com.sparkdigital.diary.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static com.sparkdigital.diary.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the CategoryMovementResource REST controller.
 *
 * @see CategoryMovementResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = DiaryApp.class)
public class CategoryMovementResourceIntTest {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    @Autowired
    private CategoryMovementRepository categoryMovementRepository;

    @Autowired
    private CategoryMovementService categoryMovementService;

    @Autowired
    private CategoryMovementSearchRepository categoryMovementSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restCategoryMovementMockMvc;

    private CategoryMovement categoryMovement;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final CategoryMovementResource categoryMovementResource = new CategoryMovementResource(categoryMovementService);
        this.restCategoryMovementMockMvc = MockMvcBuilders.standaloneSetup(categoryMovementResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static CategoryMovement createEntity(EntityManager em) {
        CategoryMovement categoryMovement = new CategoryMovement()
            .name(DEFAULT_NAME);
        return categoryMovement;
    }

    @Before
    public void initTest() {
        categoryMovementSearchRepository.deleteAll();
        categoryMovement = createEntity(em);
    }

    @Test
    @Transactional
    public void createCategoryMovement() throws Exception {
        int databaseSizeBeforeCreate = categoryMovementRepository.findAll().size();

        // Create the CategoryMovement
        restCategoryMovementMockMvc.perform(post("/api/category-movements")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(categoryMovement)))
            .andExpect(status().isCreated());

        // Validate the CategoryMovement in the database
        List<CategoryMovement> categoryMovementList = categoryMovementRepository.findAll();
        assertThat(categoryMovementList).hasSize(databaseSizeBeforeCreate + 1);
        CategoryMovement testCategoryMovement = categoryMovementList.get(categoryMovementList.size() - 1);
        assertThat(testCategoryMovement.getName()).isEqualTo(DEFAULT_NAME);

        // Validate the CategoryMovement in Elasticsearch
        CategoryMovement categoryMovementEs = categoryMovementSearchRepository.findOne(testCategoryMovement.getId());
        assertThat(categoryMovementEs).isEqualToIgnoringGivenFields(testCategoryMovement);
    }

    @Test
    @Transactional
    public void createCategoryMovementWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = categoryMovementRepository.findAll().size();

        // Create the CategoryMovement with an existing ID
        categoryMovement.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restCategoryMovementMockMvc.perform(post("/api/category-movements")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(categoryMovement)))
            .andExpect(status().isBadRequest());

        // Validate the CategoryMovement in the database
        List<CategoryMovement> categoryMovementList = categoryMovementRepository.findAll();
        assertThat(categoryMovementList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = categoryMovementRepository.findAll().size();
        // set the field null
        categoryMovement.setName(null);

        // Create the CategoryMovement, which fails.

        restCategoryMovementMockMvc.perform(post("/api/category-movements")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(categoryMovement)))
            .andExpect(status().isBadRequest());

        List<CategoryMovement> categoryMovementList = categoryMovementRepository.findAll();
        assertThat(categoryMovementList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllCategoryMovements() throws Exception {
        // Initialize the database
        categoryMovementRepository.saveAndFlush(categoryMovement);

        // Get all the categoryMovementList
        restCategoryMovementMockMvc.perform(get("/api/category-movements?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(categoryMovement.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())));
    }

    @Test
    @Transactional
    public void getCategoryMovement() throws Exception {
        // Initialize the database
        categoryMovementRepository.saveAndFlush(categoryMovement);

        // Get the categoryMovement
        restCategoryMovementMockMvc.perform(get("/api/category-movements/{id}", categoryMovement.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(categoryMovement.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingCategoryMovement() throws Exception {
        // Get the categoryMovement
        restCategoryMovementMockMvc.perform(get("/api/category-movements/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateCategoryMovement() throws Exception {
        // Initialize the database
        categoryMovementService.save(categoryMovement);

        int databaseSizeBeforeUpdate = categoryMovementRepository.findAll().size();

        // Update the categoryMovement
        CategoryMovement updatedCategoryMovement = categoryMovementRepository.findOne(categoryMovement.getId());
        // Disconnect from session so that the updates on updatedCategoryMovement are not directly saved in db
        em.detach(updatedCategoryMovement);
        updatedCategoryMovement
            .name(UPDATED_NAME);

        restCategoryMovementMockMvc.perform(put("/api/category-movements")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedCategoryMovement)))
            .andExpect(status().isOk());

        // Validate the CategoryMovement in the database
        List<CategoryMovement> categoryMovementList = categoryMovementRepository.findAll();
        assertThat(categoryMovementList).hasSize(databaseSizeBeforeUpdate);
        CategoryMovement testCategoryMovement = categoryMovementList.get(categoryMovementList.size() - 1);
        assertThat(testCategoryMovement.getName()).isEqualTo(UPDATED_NAME);

        // Validate the CategoryMovement in Elasticsearch
        CategoryMovement categoryMovementEs = categoryMovementSearchRepository.findOne(testCategoryMovement.getId());
        assertThat(categoryMovementEs).isEqualToIgnoringGivenFields(testCategoryMovement);
    }

    @Test
    @Transactional
    public void updateNonExistingCategoryMovement() throws Exception {
        int databaseSizeBeforeUpdate = categoryMovementRepository.findAll().size();

        // Create the CategoryMovement

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restCategoryMovementMockMvc.perform(put("/api/category-movements")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(categoryMovement)))
            .andExpect(status().isCreated());

        // Validate the CategoryMovement in the database
        List<CategoryMovement> categoryMovementList = categoryMovementRepository.findAll();
        assertThat(categoryMovementList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteCategoryMovement() throws Exception {
        // Initialize the database
        categoryMovementService.save(categoryMovement);

        int databaseSizeBeforeDelete = categoryMovementRepository.findAll().size();

        // Get the categoryMovement
        restCategoryMovementMockMvc.perform(delete("/api/category-movements/{id}", categoryMovement.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean categoryMovementExistsInEs = categoryMovementSearchRepository.exists(categoryMovement.getId());
        assertThat(categoryMovementExistsInEs).isFalse();

        // Validate the database is empty
        List<CategoryMovement> categoryMovementList = categoryMovementRepository.findAll();
        assertThat(categoryMovementList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchCategoryMovement() throws Exception {
        // Initialize the database
        categoryMovementService.save(categoryMovement);

        // Search the categoryMovement
        restCategoryMovementMockMvc.perform(get("/api/_search/category-movements?query=id:" + categoryMovement.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(categoryMovement.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(CategoryMovement.class);
        CategoryMovement categoryMovement1 = new CategoryMovement();
        categoryMovement1.setId(1L);
        CategoryMovement categoryMovement2 = new CategoryMovement();
        categoryMovement2.setId(categoryMovement1.getId());
        assertThat(categoryMovement1).isEqualTo(categoryMovement2);
        categoryMovement2.setId(2L);
        assertThat(categoryMovement1).isNotEqualTo(categoryMovement2);
        categoryMovement1.setId(null);
        assertThat(categoryMovement1).isNotEqualTo(categoryMovement2);
    }
}
