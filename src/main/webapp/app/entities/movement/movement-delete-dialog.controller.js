(function() {
    'use strict';

    angular
        .module('diaryApp')
        .controller('MovementDeleteController',MovementDeleteController);

    MovementDeleteController.$inject = ['$uibModalInstance', 'entity', 'Movement'];

    function MovementDeleteController($uibModalInstance, entity, Movement) {
        var vm = this;

        vm.movement = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            Movement.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
