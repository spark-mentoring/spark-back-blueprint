(function() {
    'use strict';

    angular
        .module('diaryApp')
        .controller('MovementDetailController', MovementDetailController);

    MovementDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'DataUtils', 'entity', 'Movement', 'CategoryMovement'];

    function MovementDetailController($scope, $rootScope, $stateParams, previousState, DataUtils, entity, Movement, CategoryMovement) {
        var vm = this;

        vm.movement = entity;
        vm.previousState = previousState.name;
        vm.byteSize = DataUtils.byteSize;
        vm.openFile = DataUtils.openFile;

        var unsubscribe = $rootScope.$on('diaryApp:movementUpdate', function(event, result) {
            vm.movement = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
