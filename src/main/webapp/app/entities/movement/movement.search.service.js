(function() {
    'use strict';

    angular
        .module('diaryApp')
        .factory('MovementSearch', MovementSearch);

    MovementSearch.$inject = ['$resource'];

    function MovementSearch($resource) {
        var resourceUrl =  'api/_search/movements/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }
})();
