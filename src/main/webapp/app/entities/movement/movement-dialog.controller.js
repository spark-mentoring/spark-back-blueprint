(function() {
    'use strict';

    angular
        .module('diaryApp')
        .controller('MovementDialogController', MovementDialogController);

    MovementDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', '$q', 'DataUtils', 'entity', 'Movement', 'CategoryMovement'];

    function MovementDialogController ($timeout, $scope, $stateParams, $uibModalInstance, $q, DataUtils, entity, Movement, CategoryMovement) {
        var vm = this;

        vm.movement = entity;
        vm.clear = clear;
        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;
        vm.byteSize = DataUtils.byteSize;
        vm.openFile = DataUtils.openFile;
        vm.save = save;
        vm.categorymovements = CategoryMovement.query({filter: 'movement-is-null'});
        $q.all([vm.movement.$promise, vm.categorymovements.$promise]).then(function() {
            if (!vm.movement.categoryMovement || !vm.movement.categoryMovement.id) {
                return $q.reject();
            }
            return CategoryMovement.get({id : vm.movement.categoryMovement.id}).$promise;
        }).then(function(categoryMovement) {
            vm.categorymovements.push(categoryMovement);
        });

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.movement.id !== null) {
                Movement.update(vm.movement, onSaveSuccess, onSaveError);
            } else {
                Movement.save(vm.movement, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('diaryApp:movementUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }

        vm.datePickerOpenStatus.date = false;

        function openCalendar (date) {
            vm.datePickerOpenStatus[date] = true;
        }
    }
})();
