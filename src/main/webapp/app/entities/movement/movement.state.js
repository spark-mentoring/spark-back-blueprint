(function() {
    'use strict';

    angular
        .module('diaryApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('movement', {
            parent: 'entity',
            url: '/movement',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'diaryApp.movement.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/movement/movements.html',
                    controller: 'MovementController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('movement');
                    $translatePartialLoader.addPart('movementType');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('movement-detail', {
            parent: 'movement',
            url: '/movement/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'diaryApp.movement.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/movement/movement-detail.html',
                    controller: 'MovementDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('movement');
                    $translatePartialLoader.addPart('movementType');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'Movement', function($stateParams, Movement) {
                    return Movement.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'movement',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('movement-detail.edit', {
            parent: 'movement-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/movement/movement-dialog.html',
                    controller: 'MovementDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Movement', function(Movement) {
                            return Movement.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('movement.new', {
            parent: 'movement',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/movement/movement-dialog.html',
                    controller: 'MovementDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                amount: null,
                                description: null,
                                date: null,
                                movementType: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('movement', null, { reload: 'movement' });
                }, function() {
                    $state.go('movement');
                });
            }]
        })
        .state('movement.edit', {
            parent: 'movement',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/movement/movement-dialog.html',
                    controller: 'MovementDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Movement', function(Movement) {
                            return Movement.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('movement', null, { reload: 'movement' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('movement.delete', {
            parent: 'movement',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/movement/movement-delete-dialog.html',
                    controller: 'MovementDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['Movement', function(Movement) {
                            return Movement.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('movement', null, { reload: 'movement' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
