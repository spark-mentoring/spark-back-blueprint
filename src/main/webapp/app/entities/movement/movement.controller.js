(function() {
    'use strict';

    angular
        .module('diaryApp')
        .controller('MovementController', MovementController);

    MovementController.$inject = ['DataUtils', 'Movement', 'MovementSearch'];

    function MovementController(DataUtils, Movement, MovementSearch) {

        var vm = this;

        vm.movements = [];
        vm.openFile = DataUtils.openFile;
        vm.byteSize = DataUtils.byteSize;
        vm.clear = clear;
        vm.search = search;
        vm.loadAll = loadAll;

        loadAll();

        function loadAll() {
            Movement.query(function(result) {
                vm.movements = result;
                vm.searchQuery = null;
            });
        }

        function search() {
            if (!vm.searchQuery) {
                return vm.loadAll();
            }
            MovementSearch.query({query: vm.searchQuery}, function(result) {
                vm.movements = result;
                vm.currentSearch = vm.searchQuery;
            });
        }

        function clear() {
            vm.searchQuery = null;
            loadAll();
        }    }
})();
