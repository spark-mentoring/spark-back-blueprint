(function() {
    'use strict';
    angular
        .module('diaryApp')
        .factory('Movement', Movement);

    Movement.$inject = ['$resource', 'DateUtils'];

    function Movement ($resource, DateUtils) {
        var resourceUrl =  'api/movements/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.date = DateUtils.convertDateTimeFromServer(data.date);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
