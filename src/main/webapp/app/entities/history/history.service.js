(function() {
    'use strict';
    angular
        .module('diaryApp')
        .factory('History', History);

    History.$inject = ['$resource', 'DateUtils'];

    function History ($resource, DateUtils) {
        var resourceUrl =  'api/histories/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.date = DateUtils.convertDateTimeFromServer(data.date);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
