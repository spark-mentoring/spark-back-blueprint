(function() {
    'use strict';

    angular
        .module('diaryApp')
        .factory('HistorySearch', HistorySearch);

    HistorySearch.$inject = ['$resource'];

    function HistorySearch($resource) {
        var resourceUrl =  'api/_search/histories/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }
})();
