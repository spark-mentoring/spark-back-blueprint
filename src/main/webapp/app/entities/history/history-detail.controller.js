(function() {
    'use strict';

    angular
        .module('diaryApp')
        .controller('HistoryDetailController', HistoryDetailController);

    HistoryDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'DataUtils', 'entity', 'History', 'Horse'];

    function HistoryDetailController($scope, $rootScope, $stateParams, previousState, DataUtils, entity, History, Horse) {
        var vm = this;

        vm.history = entity;
        vm.previousState = previousState.name;
        vm.byteSize = DataUtils.byteSize;
        vm.openFile = DataUtils.openFile;

        var unsubscribe = $rootScope.$on('diaryApp:historyUpdate', function(event, result) {
            vm.history = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
