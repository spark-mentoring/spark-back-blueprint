(function() {
    'use strict';

    angular
        .module('diaryApp')
        .controller('HistoryDialogController', HistoryDialogController);

    HistoryDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'DataUtils', 'entity', 'History', 'Horse'];

    function HistoryDialogController ($timeout, $scope, $stateParams, $uibModalInstance, DataUtils, entity, History, Horse) {
        var vm = this;

        vm.history = entity;
        vm.clear = clear;
        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;
        vm.byteSize = DataUtils.byteSize;
        vm.openFile = DataUtils.openFile;
        vm.save = save;
        vm.horses = Horse.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.history.id !== null) {
                History.update(vm.history, onSaveSuccess, onSaveError);
            } else {
                History.save(vm.history, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('diaryApp:historyUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }

        vm.datePickerOpenStatus.date = false;

        function openCalendar (date) {
            vm.datePickerOpenStatus[date] = true;
        }
    }
})();
