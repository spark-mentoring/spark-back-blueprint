(function() {
    'use strict';

    angular
        .module('diaryApp')
        .factory('HorseSearch', HorseSearch);

    HorseSearch.$inject = ['$resource'];

    function HorseSearch($resource) {
        var resourceUrl =  'api/_search/horses/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }
})();
