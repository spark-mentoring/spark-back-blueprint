(function() {
    'use strict';

    angular
        .module('diaryApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('horse', {
            parent: 'entity',
            url: '/horse?page&sort&search',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'diaryApp.horse.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/horse/horses.html',
                    controller: 'HorseController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }],
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('horse');
                    $translatePartialLoader.addPart('sex');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('horse-detail', {
            parent: 'horse',
            url: '/horse/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'diaryApp.horse.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/horse/horse-detail.html',
                    controller: 'HorseDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('horse');
                    $translatePartialLoader.addPart('sex');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'Horse', function($stateParams, Horse) {
                    return Horse.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'horse',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('horse-detail.edit', {
            parent: 'horse-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/horse/horse-dialog.html',
                    controller: 'HorseDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Horse', function(Horse) {
                            return Horse.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('horse.new', {
            parent: 'horse',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/horse/horse-dialog.html',
                    controller: 'HorseDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                name: null,
                                due: null,
                                description: null,
                                sex: null,
                                birth: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('horse', null, { reload: 'horse' });
                }, function() {
                    $state.go('horse');
                });
            }]
        })
        .state('horse.edit', {
            parent: 'horse',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/horse/horse-dialog.html',
                    controller: 'HorseDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Horse', function(Horse) {
                            return Horse.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('horse', null, { reload: 'horse' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('horse.delete', {
            parent: 'horse',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/horse/horse-delete-dialog.html',
                    controller: 'HorseDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['Horse', function(Horse) {
                            return Horse.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('horse', null, { reload: 'horse' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
