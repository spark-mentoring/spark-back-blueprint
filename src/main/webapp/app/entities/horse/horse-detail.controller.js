(function() {
    'use strict';

    angular
        .module('diaryApp')
        .controller('HorseDetailController', HorseDetailController);

    HorseDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'DataUtils', 'entity', 'Horse', 'History'];

    function HorseDetailController($scope, $rootScope, $stateParams, previousState, DataUtils, entity, Horse, History) {
        var vm = this;

        vm.horse = entity;
        vm.previousState = previousState.name;
        vm.byteSize = DataUtils.byteSize;
        vm.openFile = DataUtils.openFile;

        var unsubscribe = $rootScope.$on('diaryApp:horseUpdate', function(event, result) {
            vm.horse = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
