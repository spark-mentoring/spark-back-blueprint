(function() {
    'use strict';
    angular
        .module('diaryApp')
        .factory('CategoryMovement', CategoryMovement);

    CategoryMovement.$inject = ['$resource'];

    function CategoryMovement ($resource) {
        var resourceUrl =  'api/category-movements/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
