(function() {
    'use strict';

    angular
        .module('diaryApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('category-movement', {
            parent: 'entity',
            url: '/category-movement',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'diaryApp.categoryMovement.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/category-movement/category-movements.html',
                    controller: 'CategoryMovementController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('categoryMovement');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('category-movement-detail', {
            parent: 'category-movement',
            url: '/category-movement/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'diaryApp.categoryMovement.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/category-movement/category-movement-detail.html',
                    controller: 'CategoryMovementDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('categoryMovement');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'CategoryMovement', function($stateParams, CategoryMovement) {
                    return CategoryMovement.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'category-movement',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('category-movement-detail.edit', {
            parent: 'category-movement-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/category-movement/category-movement-dialog.html',
                    controller: 'CategoryMovementDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['CategoryMovement', function(CategoryMovement) {
                            return CategoryMovement.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('category-movement.new', {
            parent: 'category-movement',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/category-movement/category-movement-dialog.html',
                    controller: 'CategoryMovementDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                name: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('category-movement', null, { reload: 'category-movement' });
                }, function() {
                    $state.go('category-movement');
                });
            }]
        })
        .state('category-movement.edit', {
            parent: 'category-movement',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/category-movement/category-movement-dialog.html',
                    controller: 'CategoryMovementDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['CategoryMovement', function(CategoryMovement) {
                            return CategoryMovement.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('category-movement', null, { reload: 'category-movement' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('category-movement.delete', {
            parent: 'category-movement',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/category-movement/category-movement-delete-dialog.html',
                    controller: 'CategoryMovementDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['CategoryMovement', function(CategoryMovement) {
                            return CategoryMovement.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('category-movement', null, { reload: 'category-movement' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
