(function() {
    'use strict';

    angular
        .module('diaryApp')
        .controller('CategoryMovementDeleteController',CategoryMovementDeleteController);

    CategoryMovementDeleteController.$inject = ['$uibModalInstance', 'entity', 'CategoryMovement'];

    function CategoryMovementDeleteController($uibModalInstance, entity, CategoryMovement) {
        var vm = this;

        vm.categoryMovement = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            CategoryMovement.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
