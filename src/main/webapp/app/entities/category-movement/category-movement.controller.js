(function() {
    'use strict';

    angular
        .module('diaryApp')
        .controller('CategoryMovementController', CategoryMovementController);

    CategoryMovementController.$inject = ['CategoryMovement', 'CategoryMovementSearch'];

    function CategoryMovementController(CategoryMovement, CategoryMovementSearch) {

        var vm = this;

        vm.categoryMovements = [];
        vm.clear = clear;
        vm.search = search;
        vm.loadAll = loadAll;

        loadAll();

        function loadAll() {
            CategoryMovement.query(function(result) {
                vm.categoryMovements = result;
                vm.searchQuery = null;
            });
        }

        function search() {
            if (!vm.searchQuery) {
                return vm.loadAll();
            }
            CategoryMovementSearch.query({query: vm.searchQuery}, function(result) {
                vm.categoryMovements = result;
                vm.currentSearch = vm.searchQuery;
            });
        }

        function clear() {
            vm.searchQuery = null;
            loadAll();
        }    }
})();
