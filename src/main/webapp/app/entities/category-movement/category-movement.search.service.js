(function() {
    'use strict';

    angular
        .module('diaryApp')
        .factory('CategoryMovementSearch', CategoryMovementSearch);

    CategoryMovementSearch.$inject = ['$resource'];

    function CategoryMovementSearch($resource) {
        var resourceUrl =  'api/_search/category-movements/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true}
        });
    }
})();
