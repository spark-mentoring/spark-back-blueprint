(function() {
    'use strict';

    angular
        .module('diaryApp')
        .controller('CategoryMovementDialogController', CategoryMovementDialogController);

    CategoryMovementDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'CategoryMovement'];

    function CategoryMovementDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, CategoryMovement) {
        var vm = this;

        vm.categoryMovement = entity;
        vm.clear = clear;
        vm.save = save;

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.categoryMovement.id !== null) {
                CategoryMovement.update(vm.categoryMovement, onSaveSuccess, onSaveError);
            } else {
                CategoryMovement.save(vm.categoryMovement, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('diaryApp:categoryMovementUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
