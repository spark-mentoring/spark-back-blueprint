(function() {
    'use strict';

    angular
        .module('diaryApp')
        .controller('CategoryMovementDetailController', CategoryMovementDetailController);

    CategoryMovementDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'CategoryMovement'];

    function CategoryMovementDetailController($scope, $rootScope, $stateParams, previousState, entity, CategoryMovement) {
        var vm = this;

        vm.categoryMovement = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('diaryApp:categoryMovementUpdate', function(event, result) {
            vm.categoryMovement = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
