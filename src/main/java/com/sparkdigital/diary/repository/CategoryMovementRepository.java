package com.sparkdigital.diary.repository;

import com.sparkdigital.diary.domain.CategoryMovement;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the CategoryMovement entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CategoryMovementRepository extends JpaRepository<CategoryMovement, Long> {

}
