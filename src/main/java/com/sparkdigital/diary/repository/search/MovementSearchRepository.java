package com.sparkdigital.diary.repository.search;

import com.sparkdigital.diary.domain.Movement;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the Movement entity.
 */
public interface MovementSearchRepository extends ElasticsearchRepository<Movement, Long> {
}
