package com.sparkdigital.diary.repository.search;

import com.sparkdigital.diary.domain.CategoryMovement;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the CategoryMovement entity.
 */
public interface CategoryMovementSearchRepository extends ElasticsearchRepository<CategoryMovement, Long> {
}
