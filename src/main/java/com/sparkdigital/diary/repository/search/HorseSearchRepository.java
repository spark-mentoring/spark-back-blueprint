package com.sparkdigital.diary.repository.search;

import com.sparkdigital.diary.domain.Horse;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the Horse entity.
 */
public interface HorseSearchRepository extends ElasticsearchRepository<Horse, Long> {
}
