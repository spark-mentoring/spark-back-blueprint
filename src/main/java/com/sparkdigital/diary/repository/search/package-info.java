/**
 * Spring Data Elasticsearch repositories.
 */
package com.sparkdigital.diary.repository.search;
