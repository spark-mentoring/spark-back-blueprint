package com.sparkdigital.diary.repository;

import com.sparkdigital.diary.domain.Horse;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the Horse entity.
 */
@SuppressWarnings("unused")
@Repository
public interface HorseRepository extends JpaRepository<Horse, Long> {

}
