package com.sparkdigital.diary.domain.enumeration;

/**
 * The MovementType enumeration.
 */
public enum MovementType {
    SPEND, ENTRY
}
