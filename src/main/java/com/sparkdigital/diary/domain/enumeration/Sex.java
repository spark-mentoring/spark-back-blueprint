package com.sparkdigital.diary.domain.enumeration;

/**
 * The Sex enumeration.
 */
public enum Sex {
    MALE, FEMALE
}
