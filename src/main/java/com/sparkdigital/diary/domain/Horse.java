package com.sparkdigital.diary.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;

import javax.persistence.*;
import javax.validation.constraints.*;

import org.springframework.data.elasticsearch.annotations.Document;
import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

import com.sparkdigital.diary.domain.enumeration.Sex;

/**
 * Web page
 */
@ApiModel(description = "Web page")
@Entity
@Table(name = "horse")
@Document(indexName = "horse")
public class Horse implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Size(min = 3, max = 100)
    @Column(name = "name", length = 100, nullable = false)
    private String name;

    @Size(min = 3)
    @Column(name = "due")
    private String due;

    @Size(min = 3)
    @Lob
    @Column(name = "description")
    private String description;

    @Enumerated(EnumType.STRING)
    @Column(name = "sex")
    private Sex sex;

    @Column(name = "birth")
    private ZonedDateTime birth;

    @OneToMany(mappedBy = "horse")
    @JsonIgnore
    private Set<History> histories = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public Horse name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDue() {
        return due;
    }

    public Horse due(String due) {
        this.due = due;
        return this;
    }

    public void setDue(String due) {
        this.due = due;
    }

    public String getDescription() {
        return description;
    }

    public Horse description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Sex getSex() {
        return sex;
    }

    public Horse sex(Sex sex) {
        this.sex = sex;
        return this;
    }

    public void setSex(Sex sex) {
        this.sex = sex;
    }

    public ZonedDateTime getBirth() {
        return birth;
    }

    public Horse birth(ZonedDateTime birth) {
        this.birth = birth;
        return this;
    }

    public void setBirth(ZonedDateTime birth) {
        this.birth = birth;
    }

    public Set<History> getHistories() {
        return histories;
    }

    public Horse histories(Set<History> histories) {
        this.histories = histories;
        return this;
    }

    public Horse addHistory(History history) {
        this.histories.add(history);
        history.setHorse(this);
        return this;
    }

    public Horse removeHistory(History history) {
        this.histories.remove(history);
        history.setHorse(null);
        return this;
    }

    public void setHistories(Set<History> histories) {
        this.histories = histories;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Horse horse = (Horse) o;
        if (horse.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), horse.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Horse{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", due='" + getDue() + "'" +
            ", description='" + getDescription() + "'" +
            ", sex='" + getSex() + "'" +
            ", birth='" + getBirth() + "'" +
            "}";
    }
}
