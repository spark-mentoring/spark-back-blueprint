package com.sparkdigital.diary.service;

import com.sparkdigital.diary.domain.CategoryMovement;
import java.util.List;

/**
 * Service Interface for managing CategoryMovement.
 */
public interface CategoryMovementService {

    /**
     * Save a categoryMovement.
     *
     * @param categoryMovement the entity to save
     * @return the persisted entity
     */
    CategoryMovement save(CategoryMovement categoryMovement);

    /**
     * Get all the categoryMovements.
     *
     * @return the list of entities
     */
    List<CategoryMovement> findAll();

    /**
     * Get the "id" categoryMovement.
     *
     * @param id the id of the entity
     * @return the entity
     */
    CategoryMovement findOne(Long id);

    /**
     * Delete the "id" categoryMovement.
     *
     * @param id the id of the entity
     */
    void delete(Long id);

    /**
     * Search for the categoryMovement corresponding to the query.
     *
     * @param query the query of the search
     * 
     * @return the list of entities
     */
    List<CategoryMovement> search(String query);
}
