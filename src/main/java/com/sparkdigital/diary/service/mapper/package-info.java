/**
 * MapStruct mappers for mapping domain objects and Data Transfer Objects.
 */
package com.sparkdigital.diary.service.mapper;
