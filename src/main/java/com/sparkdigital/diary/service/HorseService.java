package com.sparkdigital.diary.service;

import com.sparkdigital.diary.domain.Horse;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing Horse.
 */
public interface HorseService {

    /**
     * Save a horse.
     *
     * @param horse the entity to save
     * @return the persisted entity
     */
    Horse save(Horse horse);

    /**
     * Get all the horses.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<Horse> findAll(Pageable pageable);

    /**
     * Get the "id" horse.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Horse findOne(Long id);

    /**
     * Delete the "id" horse.
     *
     * @param id the id of the entity
     */
    void delete(Long id);

    /**
     * Search for the horse corresponding to the query.
     *
     * @param query the query of the search
     * 
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<Horse> search(String query, Pageable pageable);
}
