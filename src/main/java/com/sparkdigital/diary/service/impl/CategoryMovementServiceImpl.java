package com.sparkdigital.diary.service.impl;

import com.sparkdigital.diary.service.CategoryMovementService;
import com.sparkdigital.diary.domain.CategoryMovement;
import com.sparkdigital.diary.repository.CategoryMovementRepository;
import com.sparkdigital.diary.repository.search.CategoryMovementSearchRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing CategoryMovement.
 */
@Service
@Transactional
public class CategoryMovementServiceImpl implements CategoryMovementService{

    private final Logger log = LoggerFactory.getLogger(CategoryMovementServiceImpl.class);

    private final CategoryMovementRepository categoryMovementRepository;

    private final CategoryMovementSearchRepository categoryMovementSearchRepository;

    public CategoryMovementServiceImpl(CategoryMovementRepository categoryMovementRepository, CategoryMovementSearchRepository categoryMovementSearchRepository) {
        this.categoryMovementRepository = categoryMovementRepository;
        this.categoryMovementSearchRepository = categoryMovementSearchRepository;
    }

    /**
     * Save a categoryMovement.
     *
     * @param categoryMovement the entity to save
     * @return the persisted entity
     */
    @Override
    public CategoryMovement save(CategoryMovement categoryMovement) {
        log.debug("Request to save CategoryMovement : {}", categoryMovement);
        CategoryMovement result = categoryMovementRepository.save(categoryMovement);
        categoryMovementSearchRepository.save(result);
        return result;
    }

    /**
     * Get all the categoryMovements.
     *
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public List<CategoryMovement> findAll() {
        log.debug("Request to get all CategoryMovements");
        return categoryMovementRepository.findAll();
    }

    /**
     * Get one categoryMovement by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public CategoryMovement findOne(Long id) {
        log.debug("Request to get CategoryMovement : {}", id);
        return categoryMovementRepository.findOne(id);
    }

    /**
     * Delete the categoryMovement by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete CategoryMovement : {}", id);
        categoryMovementRepository.delete(id);
        categoryMovementSearchRepository.delete(id);
    }

    /**
     * Search for the categoryMovement corresponding to the query.
     *
     * @param query the query of the search
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public List<CategoryMovement> search(String query) {
        log.debug("Request to search CategoryMovements for query {}", query);
        return StreamSupport
            .stream(categoryMovementSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .collect(Collectors.toList());
    }
}
