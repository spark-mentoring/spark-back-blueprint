package com.sparkdigital.diary.service.impl;

import com.sparkdigital.diary.service.HorseService;
import com.sparkdigital.diary.domain.Horse;
import com.sparkdigital.diary.repository.HorseRepository;
import com.sparkdigital.diary.repository.search.HorseSearchRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing Horse.
 */
@Service
@Transactional
public class HorseServiceImpl implements HorseService{

    private final Logger log = LoggerFactory.getLogger(HorseServiceImpl.class);

    private final HorseRepository horseRepository;

    private final HorseSearchRepository horseSearchRepository;

    public HorseServiceImpl(HorseRepository horseRepository, HorseSearchRepository horseSearchRepository) {
        this.horseRepository = horseRepository;
        this.horseSearchRepository = horseSearchRepository;
    }

    /**
     * Save a horse.
     *
     * @param horse the entity to save
     * @return the persisted entity
     */
    @Override
    public Horse save(Horse horse) {
        log.debug("Request to save Horse : {}", horse);
        Horse result = horseRepository.save(horse);
        horseSearchRepository.save(result);
        return result;
    }

    /**
     * Get all the horses.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<Horse> findAll(Pageable pageable) {
        log.debug("Request to get all Horses");
        return horseRepository.findAll(pageable);
    }

    /**
     * Get one horse by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Horse findOne(Long id) {
        log.debug("Request to get Horse : {}", id);
        return horseRepository.findOne(id);
    }

    /**
     * Delete the horse by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Horse : {}", id);
        horseRepository.delete(id);
        horseSearchRepository.delete(id);
    }

    /**
     * Search for the horse corresponding to the query.
     *
     * @param query the query of the search
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<Horse> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of Horses for query {}", query);
        Page<Horse> result = horseSearchRepository.search(queryStringQuery(query), pageable);
        return result;
    }
}
