package com.sparkdigital.diary.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.sparkdigital.diary.domain.CategoryMovement;
import com.sparkdigital.diary.service.CategoryMovementService;
import com.sparkdigital.diary.web.rest.errors.BadRequestAlertException;
import com.sparkdigital.diary.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing CategoryMovement.
 */
@RestController
@RequestMapping("/api")
public class CategoryMovementResource {

    private final Logger log = LoggerFactory.getLogger(CategoryMovementResource.class);

    private static final String ENTITY_NAME = "categoryMovement";

    private final CategoryMovementService categoryMovementService;

    public CategoryMovementResource(CategoryMovementService categoryMovementService) {
        this.categoryMovementService = categoryMovementService;
    }

    /**
     * POST  /category-movements : Create a new categoryMovement.
     *
     * @param categoryMovement the categoryMovement to create
     * @return the ResponseEntity with status 201 (Created) and with body the new categoryMovement, or with status 400 (Bad Request) if the categoryMovement has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/category-movements")
    @Timed
    public ResponseEntity<CategoryMovement> createCategoryMovement(@Valid @RequestBody CategoryMovement categoryMovement) throws URISyntaxException {
        log.debug("REST request to save CategoryMovement : {}", categoryMovement);
        if (categoryMovement.getId() != null) {
            throw new BadRequestAlertException("A new categoryMovement cannot already have an ID", ENTITY_NAME, "idexists");
        }
        CategoryMovement result = categoryMovementService.save(categoryMovement);
        return ResponseEntity.created(new URI("/api/category-movements/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /category-movements : Updates an existing categoryMovement.
     *
     * @param categoryMovement the categoryMovement to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated categoryMovement,
     * or with status 400 (Bad Request) if the categoryMovement is not valid,
     * or with status 500 (Internal Server Error) if the categoryMovement couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/category-movements")
    @Timed
    public ResponseEntity<CategoryMovement> updateCategoryMovement(@Valid @RequestBody CategoryMovement categoryMovement) throws URISyntaxException {
        log.debug("REST request to update CategoryMovement : {}", categoryMovement);
        if (categoryMovement.getId() == null) {
            return createCategoryMovement(categoryMovement);
        }
        CategoryMovement result = categoryMovementService.save(categoryMovement);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, categoryMovement.getId().toString()))
            .body(result);
    }

    /**
     * GET  /category-movements : get all the categoryMovements.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of categoryMovements in body
     */
    @GetMapping("/category-movements")
    @Timed
    public List<CategoryMovement> getAllCategoryMovements() {
        log.debug("REST request to get all CategoryMovements");
        return categoryMovementService.findAll();
        }

    /**
     * GET  /category-movements/:id : get the "id" categoryMovement.
     *
     * @param id the id of the categoryMovement to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the categoryMovement, or with status 404 (Not Found)
     */
    @GetMapping("/category-movements/{id}")
    @Timed
    public ResponseEntity<CategoryMovement> getCategoryMovement(@PathVariable Long id) {
        log.debug("REST request to get CategoryMovement : {}", id);
        CategoryMovement categoryMovement = categoryMovementService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(categoryMovement));
    }

    /**
     * DELETE  /category-movements/:id : delete the "id" categoryMovement.
     *
     * @param id the id of the categoryMovement to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/category-movements/{id}")
    @Timed
    public ResponseEntity<Void> deleteCategoryMovement(@PathVariable Long id) {
        log.debug("REST request to delete CategoryMovement : {}", id);
        categoryMovementService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/category-movements?query=:query : search for the categoryMovement corresponding
     * to the query.
     *
     * @param query the query of the categoryMovement search
     * @return the result of the search
     */
    @GetMapping("/_search/category-movements")
    @Timed
    public List<CategoryMovement> searchCategoryMovements(@RequestParam String query) {
        log.debug("REST request to search CategoryMovements for query {}", query);
        return categoryMovementService.search(query);
    }

}
