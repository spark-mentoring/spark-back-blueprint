/**
 * View Models used by Spring MVC REST controllers.
 */
package com.sparkdigital.diary.web.rest.vm;
